import React from "react";
import Router from "next/router";
import { useState, useEffect } from "react";
import '../../style/login.css'
import 'bootstrap/dist/css/bootstrap.min.css';
import Profile from '../../assets/img/brand/profile.png'
import Upload from '../../assets/img/brand/upload.png'
import { TextField } from '@material-ui/core'
import Checkbox from "@material-ui/core/Checkbox";
import InputLabel from "@material-ui/core/InputLabel";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";
import {FaTimesCircle} from 'react-icons/fa'
import { MenuProps, useStyles, options } from "../auth/util";

//import API from '../../axios'
// reactstrap components
import {
    Button,
    Card,
    CardHeader,
    CardBody,
    FormGroup,
    Form,
    Input,
    InputGroupAddon,
    InputGroupText,
    InputGroup,
    Row,
    Col,
} from "reactstrap";
// layout for this page
import Auth from "layouts/Auth.js";

function Profilesetup() {



    const [selectedFile, setSelectedFile] = useState([])
    const [profile_pic, setProfilePic] = useState('')
    const classes = useStyles();
    const [selected, setSelected] = useState([]);

    // for convert to bas64
    function imageConvertToBase64(file, callback,compare) {
        const reader = new FileReader();
        reader.onload = handleReaderLoad;
        reader.readAsDataURL(file);
        function handleReaderLoad(e) {
            if('multipleImage'== compare){
                callback([...selectedFile,e.currentTarget.result]);
            }else if('profileImage' == compare){
                callback(e.currentTarget.result)
            }
        }
    }



    // useEffect(() => {
    //     if (!selectedFile) {
    //         setPreview(undefined)
    //         return
    //     }

    //     // const objectUrl = URL.createObjectURL(selectedFile)
    //     // setPreview(objectUrl)
    //     // console.log("URL",objectUrl);
    //     // free memory when ever this component is unmounted
    //     // return () => URL.revokeObjectURL(objectUrl)

    // }, [selectedFile])

    const uploadImage = e => {
        if(selectedFile.length>=2){
            alert('bla bla bla')
            return
        }
        if (!e.target.files || e.target.files.length === 0) {
            return
        }
        // I've kept this example simple by using the first image instead of multiple
         imageConvertToBase64(e.target.files[0], setSelectedFile,'multipleImage')
        console.log(selectedFile);
        // setPreview(convertedProfile)

    }


    const profile_Image = async (e) => {
        console.log("Profile Pic");
        if (!e.target.files || e.target.files.length === 0) {
            setProfilePic(undefined)
            return
        }
        //console.log(e.target.files[0],'profile image -->>');
         imageConvertToBase64(e.target.files[0], setProfilePic,'profileImage')
        console.log(profile_pic, '====<Mmkl');
        //const objectUrl= URL.createObjectURL(profile_pic);
        // console.log("Profile URl----->",objectUrl);
    }


    const isAllSelected =
        options.length > 0 && selected.length === options.length;

    const handleChange = (event) => {
        const value = event.target.value;
        if (value[value.length - 1] === "all") {
            setSelected(selected.length === options.length ? [] : options);
            return;
        }
        setSelected(value);
        console.log(selected);
    };






    const submit = () => {
        console.log("Submit call");
        console.log(profile_pic);
        if (profile_pic) {

        }
    }

    const registerUser = async event => {
        event.preventDefault()


        /* let res = await fetch(
         'https://c487-2405-201-200f-31f9-b59d-c7a5-670e-1cf.ngrok.io/practitioner/signin',
         {
           body: JSON.stringify({
             fullName: event.target.fullname.value,
             email: event.target.emailid.value,
             phoneNumber: event.target.phonenumber.value,
             password: event.target.password.value
           }),
           headers: {
             'Content-Type': 'application/json'
           },
           method: 'POST'
         }
       ) */

        // result.user => 'Ada Lovelace'
    }

    return (
        <>
            <div className="maindiv " >
                <div className="logbox">

                    <Card className="" style={{ backgroundColor: '#121114', marginTop: '10px' }}>
                        <h1 style={{ marginTop: 10 }}>Profile</h1><br />
                        <label htmlFor="fileUpload1" onChange={profile_Image}>
                            <div style={{ align: "center", left: 150 }}  >

                                <img
                                    className="w-200"
                                    style={{ height: '111px', width: '111px', textAlign: 'center', display: 'block', marginLeft: 180 }}
                                    // src="/assets/images/illustrations/dreamer.svg"
                                    src={Profile}
                                    alt=""
                                />
                                <input hidden id="fileUpload1" type="file" accept="image/*" />
                            </div>
                        </label>
                        <CardBody className="px-lg-4 py-lg-5">


                            <Form role="form" onSubmit={registerUser}>
                                <FormGroup className="mb-3">
                                    <InputGroup>

                                        <Input
                                            style={{ backgroundColor: '#232227', border: 'none', color: '#FFFFFF', fontSize: '16px' }}
                                            placeholder="Bio"
                                            type="textarea"
                                            autoComplete="new-fullname"
                                            id="fullname"

                                        />
                                    </InputGroup>
                                </FormGroup>


                                <FormControl className={classes.formControl}>
                                    <InputLabel id="mutiple-select-label" style={{ color: '#FFFFFF' }}>Areas Of Expertise</InputLabel>
                                    <Select
                                        style={{ color: '#FFFFFF', backgroundColor: '#232227', width: '430px' }}
                                        labelId="mutiple-select-label"
                                        multiple
                                        value={selected}
                                        onChange={handleChange}
                                        renderValue={(selected) => selected.join(", ")}
                                        MenuProps={MenuProps}
                                    >
                                        <MenuItem
                                            value="all"
                                            classes={{
                                                root: isAllSelected ? classes.selectedAll : ""
                                            }}
                                        >
                                            <ListItemIcon>
                                                <Checkbox
                                                    classes={{ indeterminate: classes.indeterminateColor }}
                                                    checked={isAllSelected}
                                                    indeterminate={
                                                        selected.length > 0 && selected.length < options.length
                                                    }
                                                />
                                            </ListItemIcon>
                                            <ListItemText
                                                classes={{ primary: classes.selectAllText }}
                                                primary="Select All"
                                            />
                                        </MenuItem>
                                        {options.map((option) => (
                                            <MenuItem key={option} value={option}>
                                                <ListItemIcon>
                                                    <Checkbox checked={selected.indexOf(option) > -1} />
                                                </ListItemIcon>
                                                <ListItemText primary={option} />
                                            </MenuItem>
                                        ))}
                                    </Select>
                                </FormControl>

                                <FormGroup className="mb-3">
                                    <InputGroup className="input-group-alternative">

                                        <Input
                                            style={{ backgroundColor: '#232227', fontSize: '16px', color: "#FFFFFF", height: '40px' }}
                                            placeholder="Teacher"
                                            type="text"
                                            autoComplete="new-input"
                                            id="teacher"
                                        />
                                    </InputGroup>
                                </FormGroup>


                                {/*  <div>
                                    <input type='file' onChange={uploadImage} />
                                    {selectedFile && <img src={preview} />}
                                </div> */}

                                <div >


                                    <label htmlFor="fileUpload2" onChange={(e) => uploadImage(e)}>
                                        <div class="box alternate" style={{ align: 'center' }} >
                                            <br />
                                            <img
                                                className="w-200"
                                                style={{ height: '34px', marginLeft: '45%', width: '34px' }}
                                                // src="/assets/images/illustrations/dreamer.svg"
                                                src={Upload}
                                                alt=""
                                            />
                                            <p style={{ textAlign: 'center', height: '10px', color: '#FFEAA3', fontFamily: 'Montserrat', alignItems: 'center' }}>Upload Image</p>
                                            <p style={{ textAlign: 'center', color: '#FFFFFF', fontFamily: 'Montserrat', alignItems: 'center' }}>Supportted Format JPG/PNG/SVG</p>
                                            <input hidden id="fileUpload2" type="file" accept="image/*" />
                                        </div>
                                    </label>
                                    {/* 
                                    <p style={{ textAlign: 'center', height: '10px', color: '#FFEAA3', fontFamily: 'Montserrat', alignItems: 'center' }}>Upload Image</p>
                                    <p style={{ textAlign: 'center', color: '#FFFFFF', fontFamily: 'Montserrat', alignItems: 'center' }}>Supportted Format JPG/PNG/SVG</p> */}


                                </div>
                                {/* {selectedFile && <img style={{ color: 'red', width: '50%', height: '50%' }} src={preview} />}
                                 */}
                                 <div style={{width:'100%',height:'100px', display:'flex'}}>
                                 {selectedFile.length>0 ? selectedFile.map(data=>{
                                    return <div className="profileSetup-imagePreview"> 
                                    <img style={{position:'absolute'}} src={data} /> 
                                    <div style={{width:'100%',heigth:'100%',position:'absolute',display:'flex',justifyContent:'flex-end'}}>
                                    <FaTimesCircle style={{marginLeft:'auto',margin:"3px"}}/>
                                    </div>
                                    </div>
                                 }):null}
                                 </div> 


                                <div className="text-center">

                                    <Button className="btn1" type='submit' variant='contained' style={{ backgroundColor: '#FFEAA3', marginTop: 15, color: 'black' }} onClick={submit} >Submit </Button>
                                </div>

                            </Form>


                        </CardBody>

                    </Card>
                    {/* <Row className="mt-3">
          <Col xs="6">
            <a
              className="text-light"
              href="#pablo"
              onClick={(e) => e.preventDefault()}
            >
              <small>Forgot password?</small>
            </a>
          </Col>
        </Row> */}
                </div>
            </div>
        </>
    );
}

Profilesetup.layout = Auth;

export default Profilesetup;
