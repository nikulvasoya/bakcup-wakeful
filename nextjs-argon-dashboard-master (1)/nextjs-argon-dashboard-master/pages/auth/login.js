import React from "react";
import Router from "next/router";
import { useState } from "react";
import '../../style/login.css'
import logo from '../../assets/img/brand/sunburst.svg'
//import API from '../../axios'
// reactstrap components
import {
  Button,
  Card,
  CardHeader,
  CardBody,
  FormGroup,
  Form,
  Input,
  InputGroupAddon,
  InputGroupText,
  InputGroup,
  Row,
  Col,
} from "reactstrap";
// layout for this page
import Auth from "layouts/Auth.js";

function Login() {

  const signup = () => {
    Router.push("/auth/register");
  }
  const profilesetup = () => {
    console.log("hello");
    Router.push("/auth/profilesetup");
  }

  const login = async (event) => {
    event.preventDefault()

    console.log(event.target.email.value);
    console.log(event.target.password.value);
    try {
      var result = await fetch(
        'https://6c66-2405-201-200f-31f9-e889-37c9-25de-14f3.ngrok.io/practitioner/login',
        {
          body: JSON.stringify({

            email: event.target.email.value,
            password: event.target.password.value
          }),
          headers: {
            'Content-Type': 'application/json'
          },
          method: 'POST'
        }
      )/* .then((res) => {
        console.log("Data ==>>>",res);
      }) */.catch((err) => {
        console.log(" ==>>>", res);
      })
      console.log(result.status);
      if (result.status == 200) {
        console.log("login Sucessfully");
        profilesetup();
        //setMsg('login Sucessfully.');

      } else {
        console.log("Login Failed");
        //setMsg('Login Failed.');
      }
    } catch (err) {
      console.log("Login Error", err)
    }

    // result.user => 'Ada Lovelace'
  }
  return (
    <>
      <div className="maindiv" >
        <div className="logbox">

          <Card className="" style={{ backgroundColor: '#121114' }}>
            <div className="text-center text-muted">
              <img
                style={{ textAlign: 'center', marginTop: '50px' }}
                // src="/assets/images/illustrations/dreamer.svg"
                src={logo}
                alt=""
              />
            </div>
            <h1 style={{ marginTop: '60px' }}>Hi, Welcome Back</h1>
            <p className="p1">Enter your credentials to continue </p>
            <CardBody className="px-lg-4 py-lg-5">


              <Form role="form" onSubmit={login}>
                <FormGroup className="mb-3">
                  <InputGroup className="input-group-alternative">

                    <Input
                      style={{ backgroundColor: '#232227',color:'#FFFFFF' }}
                      placeholder="Email"
                      type="email"
                      autoComplete="new-email"
                      id="email"
                    />
                  </InputGroup>
                </FormGroup>
                <FormGroup className="mb-3">
                  <InputGroup className="input-group-alternative">

                    <Input
                      style={{ backgroundColor: '#232227',color:'#FFFFFF' }}
                      placeholder="Password"
                      type="password"
                      autoComplete="new-password"
                      id="password"
                    />
                  </InputGroup>
                </FormGroup>
                
                <div>
                  <Row className="mt-3">

                    <a
                      className="text-light"
                      href="#pablo"
                      onClick={(e) => e.preventDefault()}
                    >
                      <small style={{ color: '#FFEAA3', alignSelf: 'right', right: '10%', position: 'absolute' }}>Forgot password?</small>
                    </a>

                  </Row>

                </div> <br />
                <div className="text-center">
                  <Button className="btn1" type='submit' variant='contained' style={{ backgroundColor: '#FFEAA3', marginTop: 15, color: 'black' }} >Sign in </Button>
                </div>


              </Form>
              <Row className="mt-5" style={{ color: 'white', alignSelf: 'center', right: '35%', position: 'absolute' }}>
                <a
                  className="text-light"
                  href="#pablo"
                  onClick={signup}
                >
                  <small>Dont't have an account?</small>
                </a>
              </Row>
            </CardBody>

          </Card>
          {/* <Row className="mt-3">
          <Col xs="6">
            <a
              className="text-light"
              href="#pablo"
              onClick={(e) => e.preventDefault()}
            >
              <small>Forgot password?</small>
            </a>
          </Col>
        </Row> */}
        </div>
      </div>
    </>
  );
}

Login.layout = Auth;

export default Login;
