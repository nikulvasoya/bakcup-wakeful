import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  formControl: {
    margin: theme.spacing(1),
    width: 300
  },
  indeterminateColor: {
    color: "white"
  },
  selectAllText: {
    fontWeight: 400
  },
  selectedAll: {
    backgroundColor: "#232227",
    "&:hover": {
      backgroundColor: "#232227"
    }
  }
}));

const ITEM_HEIGHT = 38;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: '5%',
      color:'#FFFFFF',
      backgroundColor:'#232227'
    }
  },
  getContentAnchorEl: null,
  anchorOrigin: {
    vertical: "bottom",
    horizontal: "center"
  },
  transformOrigin: {
    vertical: "top",
    horizontal: "center"
  },
  variant: "menu"
};

const options = [
  "yoga 1",
  "yoga 2",
  "yoga 3",
  
];

export { useStyles, MenuProps, options };
