import React from "react";
import Router from "next/router";
import { useState } from "react";
import '../../style/login.css'
import logo from '../../assets/img/brand/sunburst.svg'
//import API from '../../axios'
// reactstrap components
import {
  Button,
  Card,
  CardHeader,
  CardBody,
  FormGroup,
  Form,
  Input,
  InputGroupAddon,
  InputGroupText,
  InputGroup,
  Row,
  Col,
} from "reactstrap";
// layout for this page
import Auth from "layouts/Auth.js";

function Register() {
  const [agree, setAgree] = useState(false);
  const checkboxHandler = () => {
    // if agree === true, it will be set to false
    // if agree === false, it will be set to true
    setAgree(!agree);
    // Don't miss the exclamation mark
  }

  const signin = () => {
    Router.push("/auth/login");
  }

  const registerUser = async event => {
    event.preventDefault()

    console.log(event.target.fullname.value);
    console.log(event.target.emailid.value);
    console.log(event.target.phonenumber.value);
    console.log(event.target.password.value);
    let res = await fetch(
      'https://6c66-2405-201-200f-31f9-e889-37c9-25de-14f3.ngrok.io/practitioner/signin',
      {
        body: JSON.stringify({
          fullName: event.target.fullname.value,
          email: event.target.emailid.value,
          phoneNumber: event.target.phonenumber.value,
          password: event.target.password.value
        }),
        headers: {
          'Content-Type': 'application/json'
        },
        method: 'POST'
      }
    )

    console.log(res);
    // result.user => 'Ada Lovelace'
  }

  return (
    <>
      <div className="maindiv" >
        <div className="logbox">

          <Card className="" style={{ backgroundColor: '#121114' }}>
            <div style={{ align: "center", display: 'flex', justifyContent: 'center', marginTop: 40 }}>
              <img
                className="w-200"
                // src="/assets/images/illustrations/dreamer.svg"
                src={logo}
                alt=""
              />
            </div>
            <div className="text-center text-muted mb-4 mt-5">

                <h1>Sign up</h1>
                <p className="p1">Sign up with Email address  </p>
              </div>
            <CardBody className="px-lg-5 py-lg-3">

              
              <Form role="form" onSubmit={registerUser}>
              <FormGroup className="mb-3">
                  <InputGroup className="input-group-alternative">

                    <Input
                      style={{ backgroundColor: '#232227',color:'#FFFFFF' }}
                      placeholder="Full Name"
                      type="text"
                      autoComplete="new-fullname"
                      id="fullname"
                    />
                  </InputGroup>
                </FormGroup>
            
                <FormGroup className="mb-3">
                  <InputGroup className="input-group-alternative">

                    <Input
                      style={{ backgroundColor: '#232227',color:'#FFFFFF' }}
                      placeholder="Email Id"
                      type="email"
                      autoComplete="new-email"
                      id="emailid"
                    />
                  </InputGroup>
                </FormGroup>

                <FormGroup className="mb-3">
                  <InputGroup className="input-group-alternative">

                    <Input
                      style={{ backgroundColor: '#232227',color:'#FFFFFF' }}
                      placeholder="Password"
                      type="password"
                      autoComplete="new-password"
                      id="password"
                    />
                  </InputGroup>
                </FormGroup>

                <FormGroup className="mb-3">
                  <InputGroup className="input-group-alternative">

                    <Input
                      style={{ backgroundColor: '#232227',color:'#FFFFFF' }}
                      placeholder="Phone Number"
                      type="text"
                      autoComplete="new-phonenumber"
                      id="phonenumber"
                    />
                  </InputGroup>
                </FormGroup>
              

                <div className="text-center">
                  <div>
                    <input style={{ float: 'left', backgroundColor: '', width: '12px', height: '12px', marginTop: 5 }} type="checkbox" id="agree" onChange={checkboxHandler} />
                    <label style={{ color: 'white', marginRight: 120 }} htmlFor="agree">Agree with terms & conditions</label>

                  </div>
                  <Button disabled={!agree} className="btn1" type='submit' variant='contained' style={{ backgroundColor: '#FFEAA3', marginTop: 15, color: 'black' }} >Next </Button>
                </div>

              </Form>

            </CardBody>
            <Row className="" style={{ color: 'white', alignSelf: 'center',marginBottom:'24px' }}>
              <a
                className="text-light"
                href="#pablo"
                onClick={signin}
              >
                <small>Already have an account?</small>
              </a>
            </Row>
          </Card>
          {/* <Row className="mt-3">
          <Col xs="6">
            <a
              className="text-light"
              href="#pablo"
              onClick={(e) => e.preventDefault()}
            >
              <small>Forgot password?</small>
            </a>
          </Col>
         </Row> */}

        </div>
      </div>
    </>
  );
}

Register.layout = Auth;

export default Register;
