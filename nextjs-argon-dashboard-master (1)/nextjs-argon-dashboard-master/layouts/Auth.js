import React from "react";
// reactstrap components
import { Container, Row, Col } from "reactstrap";

// core components
import AuthNavbar from "components/Navbars/AuthNavbar.js";
import AuthFooter from "components/Footers/AuthFooter.js";
import "../style/login.css";

import routes from "routes.js";

function Auth(props) {
  React.useEffect(() => {
    // document.body.classList.add("bg-default");
    // Specify how to clean up after this effect:
    /* return function cleanup() {
      document.body.classList.remove("bg-default");
    }; */
  }, []);
  return (
      <div className="baseDiv" >
        <>{props.children}</>
      </div>
  );
}

export default Auth;
