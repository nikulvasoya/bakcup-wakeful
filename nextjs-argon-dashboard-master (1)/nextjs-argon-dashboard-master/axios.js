
const axios = require('axios');
// import axios from 'axios.js'
// import history from 'history.js';

axios.defaults.headers.post['Access-Control-Allow-Origin'] = '*';

let axiosApi = axios.create({
  baseURL: process.env.REACT_APP_API_URL, //Staging
  timeout: 10000,
  headers: {authorization: 'foobar'},
  adapter: require('axios/lib/adapters/xhr'),
    // httpAgent: new http.Agent({keepAlive: true}),
});


// Add a request interceptor
axiosApi.interceptors.request.use(
  async config => {
    try {
      // Do something before request is sent
      // const httpMetric = perf().newHttpMetric(
      //   config.url,
      //   config.method.toUpperCase(),
      // );
      // config.metadata = {httpMetric};

      let accessToken = localStorage.getItem('accessToken');
      // let idToken = "";

      config.headers = {
        authorization: `Bearer ${accessToken}`,
        Accept: 'application/json',
        // 'Content-Type': 'application/x-www-form-urlencoded',
        'Content-Type': 'application/json',
      };
      // window.location = "/session/signin";
      // console.log("config >>", config);
      // await httpMetric.start();
    } catch (error) {
      console.log('axiosApi.interceptors.request error >>', error);
    } finally {
      return config;
    }
  },
  async error => {
    // Do something with request error
    return Promise.reject(error);
  },
);

// Add a response interceptor
axiosApi.interceptors.response.use(
  async response => {
    try {
      // Any status code that lie within the range of 2xx cause this function to trigger
      // Do something with response data

      // Request was successful, e.g. HTTP code 200
    } catch (error) {
      console.log('axiosApi.interceptors.response error >>', error);
    } finally {
      return response;
    }
  },
  async error => {
    try {
      // Any status codes that falls outside the range of 2xx cause this function to trigger
      // Do something with response error
      const originalRequest = error.config;
      // if (error.response.status === 403 && !originalRequest._retry) {
      //   originalRequest._retry = true;
      //   const access_token = await refreshAccessToken();
      //   axios.defaults.headers.common['Authorization'] = 'Bearer ' + access_token;
      //   return axiosApi(originalRequest);
      // }

      if (error.request && error.request.status === 401) {
        // await AsyncStorage.removeItem('idToken');
        // NavigationService.navigate('Login');
        // history.push('/session/signin')
        localStorage.removeItem('accessToken')
        localStorage.removeItem('user')
        delete axios.defaults.headers.common.authorization
        window.location = "/session/signin";
      }
    } catch (error) {
      console.log('axiosApi.interceptors.response error >>', error);
    } finally {
      return Promise.reject(error);
    }
  },
);

const refreshAccessToken = () => {};

// module.exports = axiosApi;
export default axiosApi;
